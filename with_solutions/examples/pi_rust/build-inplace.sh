#!/bin/bash

DEST="${DEST:-./}"
PY_V="${PY_V:-"$(python --version | grep -Po "Python \K[0-9]\.[0-9]*" | tr -d '.')m"}"

target="release"
cargo_arg="--release"
[[ "$1" == "--debug" ]] && target="debug" && cargo_arg=""

project=$(grep -Po '^name ?= ?"\K[^"]*' Cargo.toml | tail -n1)

cargo build $cargo_arg
cp target/${target}/lib${project}.so "${DEST}/${project}.cpython-${PY_V}-x86_64-linux-gnu.so"
