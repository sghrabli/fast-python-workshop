### Euclidean distance matrix

https://en.wikipedia.org/wiki/Euclidean_distance_matrix

### Compute π with Monte Carlo methods

text - https://www.geeksforgeeks.org/estimating-value-pi-using-monte-carlo/

text - https://academo.org/demos/estimating-pi-monte-carlo/

Pics and info -https://www.kaggle.com/nickgould/monte-carlo-tutorial-calculating-pi

### Grid search

https://towardsdatascience.com/grid-search-for-model-tuning-3319b259367e

https://en.wikipedia.org/wiki/Hyperparameter_optimization#Grid_search

### Counting words

https://github.com/minrk/IPython-parallel-tutorial/blob/42ae138529d652d7f18925c619dd305dde579a2f/examples/Counting%20Words.ipynb

https://en.wikipedia.org/wiki/N-gram

### Big O Notation

https://en.wikipedia.org/wiki/Big_O_notation

pics - https://runestone.academy/runestone/books/published/pythonds/AlgorithmAnalysis/BigONotation.html

### Avoid unnecessary computations

https://en.wikipedia.org/wiki/Fibonacci_number

https://dbader.org/blog/python-memoization

https://docs.python.org/3/library/functools.html

https://joblib.readthedocs.io/en/latest/

### Cython

https://cython.readthedocs.io/en/latest/src/tutorial/cython_tutorial.html

https://cython.org/

### Numba

https://numba.readthedocs.io/en/stable/user/5minguide.html

### Divide-and-conquer algorithm

https://en.wikipedia.org/wiki/Divide-and-conquer_algorithm

https://en.wikipedia.org/wiki/Merge_sort

https://www.programmersought.com/article/9153365366/

https://en.wikipedia.org/wiki/MapReduce

https://www.todaysoftmag.com/article/1358/hadoop-mapreduce-deep-diving-and-tuning

### Amdahl's Law

https://en.wikipedia.org/wiki/Amdahl%27s_law

### Algorithm

https://en.wikipedia.org/wiki/Bubble_sort

https://en.wikipedia.org/wiki/Merge_sort

https://www.geeksforgeeks.org/merge-sort/

### Data Structures

https://www.geeksforgeeks.org/data-structures/

https://en.wikipedia.org/wiki/Data_structure

https://medium.com/@lucasmagnum/sidenotes-array-abstract-data-type-data-structure-b154140c8305

https://www.geeksforgeeks.org/stack-in-python/

### NumPy
https://en.wikipedia.org/wiki/NumPy

https://github.com/numpy/numpy

https://www.w3schools.com/python/numpy_intro.asp

### Pandas
https://pandas.pydata.org/about/index.html

### Binning

https://www.geeksforgeeks.org/binning-in-data-mining/

https://en.wikipedia.org/wiki/Data_binning

### Swap Space in Operating System

https://www.geeksforgeeks.org/swap-space-in-operating-system/

### Sparse matrix

https://en.wikipedia.org/wiki/Sparse_matrix#:~:text=In%20numerical%20analysis%20and%20scientific,the%20matrix%20is%20considered%20dense

https://pandas.pydata.org/pandas-docs/stable/user_guide/sparse.html

### Real numbers

http://www.cs.appstate.edu/~aam/classes/1100/cn/sect1_4.html

### Offline computation

https://en.wikipedia.org/wiki/Online_algorithm

https://stackoverflow.com/questions/11496013/what-is-the-difference-between-an-on-line-and-off-line-algorithm

### Generators

https://www.geeksforgeeks.org/generators-in-python/?ref=lbp

https://www.geeksforgeeks.org/using-generators-for-substantial-memory-savings-in-python/?ref=rp